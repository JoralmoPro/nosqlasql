# SQL a NoSQL ¿Cómo? 



## Antes que nada se debe pensar en

- ## Almacenamiento de datos

- ## Operaciones realizadas (Lectura/Escritura)

- ## Recuperación de datos

### Se tratara de responder ¿Cómo hacer relaciones de datos en bases de datos NoSQL?



### ¿Con que estamos tratando?

##### *"Cloud Firestore es una base de datos flexible y escalable para la programación en servidores, dispositivos móviles y la Web desde Firebase y Google Cloud Platform. Al igual que Firebase Realtime Database, mantiene tus datos sincronizados entre apps cliente a través de agentes de escucha en tiempo real y ofrece asistencia sin conexión para dispositivos móviles y la Web, por lo que puedes compilar apps con capacidad de respuesta que funcionan sin importar la latencia de la red ni la conectividad a Internet. Cloud Firestore también ofrece una integración sin interrupciones con otros productos de Firebase y Google Cloud Platform, incluido Cloud Functions."*



# Ejemplo de modelo de datos relacional

![modeloBDrelacional](https://gitlab.com/JoralmoPro/nosqlasql/raw/master/modeloBDrelacional.png)

### Podemos notar conceptos como relaciones, normalización.

## Ejemplo de consulta para leer datos de una persona

`SELECT p.Nombre, p.Apellido, d.Ciudad, dc.Detalle
FROM Persona as p
JOIN DetalleContacto as dc ON dc.idPersona = p.id
JOIN Direccion as d ON d.idPersona = p.id`

### Podemos notar que la lectura o escritura sobre la información de una sola persona requiere de operaciones en varias tablas



## Ahora veamos un ejemplo de un modelamiento de los mismos datos pero en una base de datos NoSQL

`{
    "id": "1",
    "Nombre": "Jose",
    "Altamar": "Altamar",
    "Dirección": [
        {            
            "Dirección": "Calle 28 Carrera 18a",
            "Ciudad": "Barranquilla",
            "Departamento": "Atlantico",
            "CodigoPostal": "478001",
            "Pais": "Colombia"
        }
    ],
    "DetallesDeContacto": [
        {"correo": "JoralmoPro@gmail.com"},
        {"celular": "3002892012", "de":"+57"}
    ] 
}`

#### Podemos notar que se ha perdido la normalización, y utilizado algo conocido como "*incrustación*", para modelar toda la información relacionada con una persona en un solo documento JSON; tambien se cuenta con la flexibilidad para tener información personalizada por cada registro de persona.

##### Por lo que de esté modo, recuperar los datos de una persona es cuestión de hacer solamente una operación de lectura de un único documento, al igual que una operación de escritura.

##### Esto se hace con el fin de emitir menos consultas y actualizaciones para realizar operaciones frecuentes.

## ¡Pero hay que tener cuidado!

#### ¿Con qué? con la incrustación

##### solo es recomendable usarlas cuando existen datos que cambian con poca frecuenta o es controlable la cantidad de datos incrustado (hay un limite de crecimiento)

## Por ejemplo

#### Veamos algunas situaciones donde debemos evitar la inscrustación de datos

###### Se tiene el siguiente documento

`{
    "id": "1",
    "nombre": "Que es SQL",
    "Resumen": "Un post para tratar la definición de SQL",
    "Comentarios": [
        {"id": 1, "autor": "anonimo", "comentario": "Muchas gracias"},
        {"id": 2, "autor": "bob", "comentario": "Muy claro el asunto"},
        …
        {"id": 100001, "autor": "jane", "comentario": "Me sirvió para mi tarea"},
        …
        {"id": 1000000001, "autor": "angry", "comentario": "Le falto mas ejemplos"},
        …
        {"id": ∞ + 1, "autor": "bored", "comentario": "Gracias"},
    ]
}`

###### No hay limites (comentarios)

###### Lo ideal sería

`Posts:
{
    "id": "1",
    "nombre": "Que es SQL",
    "Resumen": "Un post para tratar la definición de SQL",
    "comentariosRecientes": [
        {"id": 1, "autor": "anonimo", "comentario": "Muchas gracias"},
        {"id": 2, "autor": "bob", "comentario": "Muy claro el asunto"},
        {"id": 3, "autor": "jane", "comentario": "....."}
    ]
}`

`Comentarios:
{
    "postId": "1"
    "comentarios": [
        {"id": 4, "autor": "anonimo", "comentario": "gracias"},
        {"id": 5, "autor": "bob", "comentario": "Bien detallado"},
        ...
        {"id": 99, "autor": "angry", "comentario": "Que nivel"}
    ]
},
{
    "postId": "1"
    "comentarios": [
        {"id": 100, "autor": "anon", "comentario": "Cuando tratas sobre NoSQL?"},
        ...
        {"id": 199, "autor": "bored", "comentario": "No me quedó claro"}
    ]
}`

#### Organiza mejor los datos, 3 ultimos comentarios en el documentos de posts, agrupación de comentarios de 100 en 100.



### Tampoco se recomienda incrustar datos cuando

##### los datos incrustados se utilizan a menudo, o cambian con frecuencia, por ejemplo

###### Se tiene el siguiente documento

`{
    "id": "1",
    "Nombre": "Jose",
    "Apellido": "Altamar",
    "cambiosEnCartera": [
        {
            "retenidos": 100,
            "stock": { "id": "zaza", "abierto": 1, "valorMasAlto": 2, "valorMasBajo": 0.5 }
        },
        {
            "retenidos": 50,
            "stock": { "id": "xc", "abierto": 89, "valorMasAlto": 93, "valorMasBajo": 88.87 }
        }
    ]
}`

#### Representa las acciones de una persona, en lo cual las acciones cambian con frecuencia, y donde tambien es posible que muchos usuarios negoceen la misma acción (zaza por ejemplo) muchas veces al día.



## Para estos casos, podemos usar "Datos de referencia", para simular una relación entre documentos al momento de necesitar alguna información de aquí o de allá

#### Retomando el ejemplo anterior

`Persona:
{
    "id": "1",
    "Nombre": "Jose",
    "Apellido": "Altamar",
    "cartera": [
        { "retenidos":  100, "stockId": 1},
        { "retenidos":  50, "stockId": 2}
    ]
}`

`Stock:
{
    "id": "1",
    "simbolo": "zaza",
    "abierto": 1,
    "valorMasAlto": 2,
    "valorMasBajo": 0.5,
    "vol": 11970000,
    "mkt-cap": 42000000,
    "pe": 5.89
},
{
    "id": "2",
    "simbolo": "xc",
    "abierto": 89,
    "valorMasAlto": 93.24,
    "valorMasBajo": 88.87,
    "vol": 2970200,
    "mkt-cap": 1005000,
    "pe": 75.82
}`

##### Se mejoran las operaciones de escritura, pero se comprometen un poco las operaciones de lectura (aunque estas tienen un menor impacto en el rendimiento)



## ¿Como saber en que documento colocar la referencia a otro documento?

### Viendo el futuro

##### Dependiendo como crezca la relación, determinaremos en que documento colocar la referencia a otro

###### Por ejemplo

`Editor:
{
    "id": "joralmopro",
    "nombre": "Jose Altamar",
    "libros": [ 1, 2, 3, ..., 100, ..., 1000]
}`

`Libros:
{"id": "1", "nombre": "Un libro" }
{"id": "2", "nombre": "Otro libro" }
{"id": "3", "nombre": "Titulo de un libro" }
...
{"id": "100", "nombre": "Soy un libro" }
...
{"id": "1000", "nombre": "Otro librito" }`

##### Tambien entra en juego el limite de registros, como el caso de los libros

###### Por lo que sería mejor

`Editor: 
{
    "id": "joralmopro",
    "nombre": "JoralmoPro"
}`

`Libros: 
{"id": "1","nombre": "Un libro", "pub-id": "joralmopro"}
{"id": "2","nombre": "Otro libro", "pub-id": "joralmopro"}
{"id": "3","nombre": "Titulo de un libro"}
...
{"id": "100","nombre": "Soy un libro", "pub-id": "joralmopro"}
...
{"id": "1000","nombre": "Otro librito", "pub-id": "joralmopro"}`

##### Mismo resultado, diferente rendimiento



# ¿Como modelariamos una relacion de muchos a muchos?

![muchosAmuchosrelacional](https://gitlab.com/JoralmoPro/nosqlasql/raw/master/muchosAmuchosrelacional.png)

### Podriamos hacer lo siguiente

`Autores: 
{"id": "a1", "nombre": "Jose Altamar" }
{"id": "a2", "nombre": "Rafael Molina" }`

`Libros:
{"id": "b1", "nombre": "Un libro" }
{"id": "b2", "nombre": "Otro libro" }
{"id": "b3", "nombre": "Titulo de un libro" }
{"id": "b4", "nombre": "Titulo de otro libro" }
{"id": "b5", "nombre": "Estó es un titulo" }`

`Uniones: 
{"autorId": "a1", "libroId": "b1" }
{"autorId": "a2", "libroId": "b1" }
{"autorId": "a1", "libroId": "b2" }
{"autorId": "a1", "libroId": "b3" }`

### Lo cual funcionaría pero tendría problemas, como cargar un autor con sus libros se requeriría por lo menos dos consultas a la base de datos adicionales.

#### Pero de mejor manera, podríamos obviar la tabla de "Uniones" quedando de la siguiente manera

`Autores:
{"id": "a1", "nombre": "Jose Altamar", "libros": ["b1, "b2", "b3"]}
{"id": "a2", "nombre": "Rafael Molina", "libros": ["b1", "b4"]}`

`Libros: 
{"id": "b1", "nombre": "Un libro", "autores": ["a1", "a2"]}
{"id": "b2", "nombre": "Otro libro", "autores": ["a1"]}
{"id": "b3", "nombre": "Titulo de un libro", "autores": ["a1"]}
{"id": "b4", "nombre": "Titulo de otro libro", "autores": ["a2"]}`

### De este modo se puede tener la información de manera mas inmediata.



# Juntando los 2 conceptos, Modelos de datos híbridos

### Hay casos especificos en los que se pueden juntar los dos conceptos vistos anteriormente, incrustación y usar referencias, generando una lógica mas simple en la aplicación, ahorrando consultas.

###### Se tiene lo siguiente

`Autores: 
{
    "id": "a1",
    "nombre": "Jose",
    "apellido": "Altamar",        
    "numeroDeLibros": 3,
    "libros": ["b1", "b2", "b3"],
    "imagenes": [
        {"thumbnail": "http://....png"}
        {"perfil": "http://....png"}
        {"grande": "http://....png"}
    ]
},
{
    "id": "a2",
    "nombre": "Rafael",
    "apellido": "Molina",
    "numeroDeLibros": 1,
    "libros": ["b1"],
    "imagenes": [
        {"thumbnail": "http://....png"}
    ]
}`

`Libros:
{
    "id": "b1",
    "nombre": "Un libro",
    "autores": [
        {"id": "a1", "nombre": "Jose Altmar", "thumbnailUrl": "http://....png"},
        {"id": "a2", "nombre": "Rafael Molina", "thumbnailUrl": "http://....png"}
    ]
},
{
    "id": "b2",
    "nombre": "Otro libro",
    "autores": [
        {"id": "a1", "nombre": "Jose Altamar", "thumbnailUrl": "http://....png"},
    ]
}`

#### Cómo vemos se combinan ambas formas de armar los documentos





# ¡Gracias!

###### Preguntas/Dudas @JoralmoPro

###### *Documento de referencia [Aquí](https://docs.microsoft.com/en-us/azure/cosmos-db/modeling-data)*

###### *Este documento está alojado en [GitLab](http://bit.ly/sqlanosql)*

